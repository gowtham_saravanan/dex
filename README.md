# DEX
A DEX like uniswap to learn and master DeFi.

This projects allows to
- Create liquidity pairs
- Add or remove liquidity
- Swap tokens
    - Swap Exact Tokens In For Tokens
    - Swap Exact ETH In For Tokens
    - Swap Exact Tokens In For ETH
    - Swap Tokens In For Exact Tokens
    - Swap ETH In For Exact Tokens
    - Swap Tokens In For Exact ETH
- Get no of B tokens out for A tokens in 
- Get no of A tokens needed for B tokens out

### Note:
- This project is for learning purposes, any suggestions on security and gas improvements are welcome.

### Upcoming:   
- Detailed documentation including natspec are to be implemented soon.
- Token swap React Frontend to be included soon.

### How to test

##### Setting up mainnet fork using hardhat
- Navigate to smart-contracts folder using `cd .\smart-contracts\`
- Fork the mainnet using hardhat
    `npx hardhat node --fork https://eth-mainnet.alchemyapi.io/v2/{ALCHEMY_MAINET_API_KEY}`. After few minutes the ethereum mainnet fork starts running on localhost 

##### Test cases execution
- Open a new terminal and navigate to smart-contracts folder using `cd .\smart-contracts\`, so the ethereum fork is not disturbed.  
- Then run the test cases using `npx hardhat test --network localhost`
![DEX test cases images](/images/dex-test-cases.png)
