// SPDX-License-Identifier: MIT
pragma solidity 0.8.17;

interface IFactory {

    function getPair(address token0, address token1) external view returns(address);

    function allPairs() external view returns(address[] memory);
    
    function totalPairs() external view returns(uint);

    function createPair(address, address) external returns(address);

}