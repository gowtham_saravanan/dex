// SPDX-License-Identifier: MIT

pragma solidity 0.8.17;

interface IERC20Token {

    function transferFrom(address from, address to, uint amount) external returns(bool);
    
    function transfer(address to, uint amount) external returns(bool);
    
    function approve(address to, uint amount) external;

    function allowance(address owner, address spender) external view returns (uint256);
    
    function balanceOf(address owner) view external returns(uint);

    function permit(address from, address to, uint amount, uint deadline, uint8 v, bytes32 r, bytes32 s) external;

    function nonces(address owner) view external returns(uint);

    function DOMAIN_SEPARATOR() view external returns(bytes32);

    function PERMIT_TYPEHASH() view external returns(bytes32);

}