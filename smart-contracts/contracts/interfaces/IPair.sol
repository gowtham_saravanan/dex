// SPDX-License-Identifier: MIT
pragma solidity 0.8.17;

interface IPair {

    function initialize(address _token0, address _token1) external;

    function getReserves(address, address) external view returns(uint, uint);

    function getTokens() external view returns(address, address);

    function mint(address to, uint amountA, uint amountB) external returns (uint);

    function burn(address from, uint liquidity) external returns(uint, uint);

    function swap(address tokenIn, uint amount, address to) external returns(uint);

}