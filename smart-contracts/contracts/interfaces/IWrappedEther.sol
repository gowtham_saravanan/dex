// SPDX-License-Identifier: MIT
pragma solidity 0.8.17;

import "./IERC20Token.sol";

interface IWrappedEther is IERC20Token {
    function deposit() external payable;
    function withdraw(uint amount) external;
}