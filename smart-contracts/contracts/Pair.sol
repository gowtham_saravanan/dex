// SPDX-License-Identifier: MIT

pragma solidity 0.8.17;

import './ERC20Token.sol';
import './libraries/TransferHelper.sol';
import './libraries/SwapHelper.sol';
import './interfaces/IFactory.sol';
import '@openzeppelin/contracts/utils/math/Math.sol';
import '@openzeppelin/contracts/security/ReentrancyGuard.sol';

contract Pair is ERC20Token {

    address private token0;
    address private token1;
    address private immutable factory;

    uint private reserve0;
    uint private reserve1;
    
    constructor() {
        factory = msg.sender;
    }
    
    function initialize(address _token0, address _token1) external {
        require(msg.sender == factory, "Only factory can initialize");
        (token0, token1) = (_token0, _token1);
    }

    function getReserves(address _tokenA, address _tokenB) public view returns(uint reserveA, uint reserveB){
        (reserveA, reserveB) =  (_tokenA == token0) ? (reserve0, reserve1) : (reserve1, reserve0);
    }

    function getTokens() public view returns(address, address) {
        return (token0, token1);
    }

    /*
        Add's liquidity to the pair. Checks if the specified amount of tokens are deposited 
        and mints the shares.
    */
    function mint(address _to, uint _amountA, uint _amountB) external nonReentrant returns(uint liquidity) {
        uint _totalSupply = totalSupply();

        // Check for the token balances and if dx/dy == X/Y
        require(_amountA > 0 && _amountB > 0, "Not enough tokens provided");
        require(IERC20Token(token0).balanceOf(address(this)) - reserve0 >= _amountA, "Not enough tokenA provided");
        require(IERC20Token(token1).balanceOf(address(this)) - reserve1 >= _amountB, "Not enough tokenB provided");
        require(_amountA * reserve1 == _amountB * reserve0, "dx/dy != X/Y");

        // Find shares to mint
        if(_totalSupply == 0){
            // s = sqrt(dx*dy)
            liquidity = Math.sqrt(_amountA * _amountB);
        } else {
            // s = dxT/X = dyT/Y 
            liquidity = Math.min((_amountA * _totalSupply)/reserve0, (_amountB*_totalSupply)/reserve1);
        }
        require(liquidity > 0, "Liquidity is zero");

        // Mint shares and update reserves
        _mint(_to, liquidity);
        _updateReserves(IERC20(token0).balanceOf(address(this)), IERC20(token1).balanceOf(address(this)));
    }

    /*
        Burn's the liquidity in the pair and returns the tokens.
    */
    function burn(address _to, uint _liquidity) external nonReentrant returns(uint amountA, uint amountB) {
        // Find the amount of tokens to be transferred for the provided liquidity shares
        amountA = (_liquidity * reserve0) / totalSupply();
        amountB = (_liquidity * reserve1) / totalSupply();
        require(amountA > 0 && amountB > 0, "Insufficient tokens");

        // Transfer the tokens
        TransferHelper.transfer(token0, _to, amountA, "Unable to send tokenA");
        TransferHelper.transfer(token1, _to, amountB, "Unable to send tokenB");

        // Burn the liquidity and update the reserves 
        _burn(address(this), _liquidity);
        _updateReserves(IERC20(token0).balanceOf(address(this)), IERC20(token1).balanceOf(address(this)));
    }

    function swap(address _token, uint _amount, address _to) external nonReentrant returns(uint amountOut) {
        require(_token == token0 || _token == token1, "Invalid token type");
        require(_amount > 0, "Invalid amount");
        
        (address tokenIn, address tokenOut, uint reserveIn, uint reserveOut) = 
        (token0 == _token) ? (token0, token1, reserve0, reserve1) : (token1, token0, reserve1, reserve0);

        require(IERC20Token(tokenIn).balanceOf(address(this)) >= reserveIn + _amount);
        amountOut = SwapHelper.getAmountOut(_amount, reserveIn, reserveOut);
        require(amountOut < reserveOut, "Insufficient token reserves");
        TransferHelper.transfer(tokenOut, _to , amountOut, "Failed to transfer output tokens");
    
        _updateReserves(IERC20(token0).balanceOf(address(this)), IERC20(token1).balanceOf(address(this)));            
    }
    
    function _updateReserves(uint _reserve0, uint _reserve1) private {
        (reserve0, reserve1) = (_reserve0, _reserve1);
    }
}