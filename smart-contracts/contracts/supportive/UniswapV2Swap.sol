// SPDX-License-Identifier: MIT
pragma solidity 0.8.17;

interface IUniswapV2Router {
    function swapExactTokensForTokens(
        uint amountIn,
        uint amountOutMin,
        address[] calldata path,
        address to,
        uint deadline
    ) external returns (uint[] memory amounts);
}

interface IUniswapV2Factory {
    function getPair(address token0, address token1) external view returns (address);
}

interface IUniswapPair {
    function swap (
        uint amount0Out,
        uint amount1Out,
        address to,
        bytes calldata data
    ) external;
}

interface IERC20 {
    function transferFrom(address from, address to, uint amount) external;
    function transfer(address to, uint amount) external;
    function balanceOf(address account) external view returns(uint) ;
    function approve(address to, uint amount) external returns(bool);
     function allowance(address owner, address spender) external view returns (uint256);
}

interface IWETH is IERC20 {
    function deposit() external payable;
    function withdraw(uint amount) external; 
}

contract UniswapV2Swap {

    address private constant UNISWAP_ROUTER = 0x7a250d5630B4cF539739dF2C5dAcb4c659F2488D;
    address private constant FACTORY = 0x5C69bEe701ef814a2B6a3EDD4B1652CB9cc5aA6f;
    address private constant WETH = 0xC02aaA39b223FE8D0A0e5C4F27eAD9083C756Cc2;
    address private constant USDC = 0xA0b86991c6218b36c1d19D4a2e9Eb0cE3606eB48;
    
    IUniswapV2Router private router = IUniswapV2Router(UNISWAP_ROUTER);
    IERC20 private weth = IERC20(WETH);
    IERC20 private usdc = IERC20(USDC);
    
    function singleSwapExactAmountIn(uint _amountIn, uint _minAmountOut) external returns(uint) {
        weth.transferFrom(msg.sender, address(this), _amountIn);
        weth.approve(address(router), _amountIn);

        address[] memory path = new address[](2);
        path[0] = WETH;
        path[1] = USDC;

        uint[] memory amounts = router.swapExactTokensForTokens(_amountIn, _minAmountOut, path, msg.sender, block.timestamp);

        return amounts[1];
    }

}