// SPDX-License-Identifier: MIT
pragma solidity 0.8.17;

import "./Pair.sol";
import './interfaces/IPair.sol';

contract Factory{

    mapping(address => mapping(address => address)) public getPair;
    address[] public allPairs;

    function totalPairs() external view returns(uint){
        return allPairs.length;
    }

    function createPair(address _tokenA, address _tokenB) external returns(address pair) {
        require(_tokenA != _tokenB, "Both tokens are same");
        require(_tokenA != address(0) && _tokenB != address(0), "Token address cannot be zero");
        (address token0, address token1) = (_tokenA < _tokenB) ? (_tokenA, _tokenB) : (_tokenB, _tokenA);
        require(getPair[token0][token1] == address(0), "Pair already exists");

        bytes32 _salt = keccak256(abi.encodePacked(token0, token1));
        pair = address(new Pair{salt: _salt}());        
        IPair(pair).initialize(token0, token1);
        
        getPair[token0][token1] = pair;
        getPair[token1][token0] = pair;
        allPairs.push(pair);
    }
    
}