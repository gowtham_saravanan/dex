// SPDX-License-Identifier: MIT
pragma solidity 0.8.17;

import './interfaces/IPair.sol';
import './interfaces/IFactory.sol';
import './interfaces/IWrappedEther.sol';
import './libraries/TransferHelper.sol';
import './libraries/SwapHelper.sol';

contract Router {
    address public immutable factory;
    address public immutable WETH;

    modifier isActive(uint deadline){
        require(block.timestamp <= deadline, "Time expired");
        _;
    }

    constructor (address _factory, address _WETH) {
        factory = _factory;
        WETH = _WETH;
    }

    receive() external payable {
        assert(msg.sender == WETH);
    }

    /*
        Swap's the exact tokens in for tokens out. Fails if the final output token is less the _amountOutMin.
        It allows mutiple swaps in the order of the param _path.
    */
    function swapExactTokensInForTokens(
        uint _amountIn,
        uint _amountOutMin,
        address[] calldata _path,
        address _to,
        uint _deadline
    ) external isActive(_deadline) returns(uint[] memory amounts){
        // Get the predicted amounts and checks if it satisfies the minimum amount out.
        uint[] memory predictedAmounts = SwapHelper.getAmountsOut(factory, _amountIn, _path);
        require(predictedAmounts[_path.length - 1] >= _amountOutMin, "Insufficient amount out tokens");
        
        // Swap the tokens according to specified path and amountIn
        amounts = _swap(_amountIn, _path, msg.sender, _to);
    }

    /*
        Swap's the exact ETH in for tokens out. Fails if the final output token is less the _amountOutMin.
        It allows mutiple swaps in the order of the param _path.
    */
    function swapExactETHInForTokens(
        uint _amountOutMin,
        address[] calldata _path,
        address _to,
        uint _deadline
    ) external payable isActive(_deadline) returns(uint[] memory amounts){
        require(_path[0] == WETH, "Invalid Path");

        // Get the predicted amounts and checks if it satisfies the minimum amount out.
        uint[] memory predictedAmounts = SwapHelper.getAmountsOut(factory, msg.value, _path);
        require(predictedAmounts[_path.length-1] >= _amountOutMin, "Insufficient amount out tokens");
        
        // Deposit the WETH and Swap the tokens according to specified path and amountIn
        IWrappedEther(WETH).deposit{value: msg.value}();
        amounts = _swap(msg.value, _path, address(this), _to);
    }

    /*
        Swap's the exact tokens in ETH. Fails if the final output ETH is less the _amountOutMin.
        It allows mutiple swaps in the order of the param _path.
    */
    function swapExactTokensInForETH(
        uint _amountIn,
        uint _amountOutMin,
        address[] calldata _path,
        address _to,
        uint _deadline
    ) external isActive(_deadline) returns(uint[] memory amounts){
        require(_path[_path.length - 1] == WETH, "Invalid Path");

        // Get the predicted amounts and checks if it satisfies the minimum amount out.
        uint[] memory predictedAmounts = SwapHelper.getAmountsOut(factory, _amountIn, _path);
        require(predictedAmounts[_path.length - 1] >= _amountOutMin, "Insufficient amount out tokens");
        
        // Swap the tokens according to specified path and amountIn
        amounts = _swap(_amountIn, _path, msg.sender, address(this));

        // Withdraw the WETH and transfer to the _to address
        IWrappedEther(WETH).withdraw(amounts[_path.length - 1]);
        TransferHelper.transferETH(_to, amounts[_path.length - 1]);
    }
    
    /*
        Swap's the tokens in for exact tokens out. Fails if the required tokens in are greater
        than _amountInMax. It allows mutiple swaps in the order of the param _path.
    */
    function swapTokensInForExactTokens(
        uint _amountOut,
        uint _amountInMax,
        address[] calldata _path,
        address _to,
        uint _deadline
    ) external isActive(_deadline) returns(uint[] memory amounts){

        // Get the predicted amounts and checks if it is lesser than _amountInMax.
        uint[] memory predictedAmounts = SwapHelper.getAmountsIn(factory ,_amountOut, _path);
        require(predictedAmounts[0] <= _amountInMax, "Exceeds the maximum input tokens");
        
        // Swap the tokens according to specified path and predicted amountIn
        amounts = _swap(predictedAmounts[0], _path, msg.sender, _to);
    }

    /*
        Swap's the ETH in for exact tokens out. Fails if the required ETH in is greater
        than ETH sent. It allows mutiple swaps in the order of the param _path.
    */
    function swapETHInForExactTokens(
        uint _amountOut,
        address[] calldata _path,
        address _to,
        uint _deadline
    ) external payable isActive(_deadline) returns(uint[] memory amounts){
        require(_path[0] == WETH, "Invalid Path");
        
        // Get the predicted amounts and checks if it's lesser than ETH sent.
        uint[] memory predictedAmounts = SwapHelper.getAmountsIn(factory ,_amountOut, _path);
        require(predictedAmounts[0] <= msg.value, "Exceeds the maximum input tokens");
        
        // Deposit the wrapped ether and swap the tokens according to the _path
        IWrappedEther(WETH).deposit{value: predictedAmounts[0]}();
        amounts = _swap(predictedAmounts[0], _path, address(this), _to);

        // Send back the extra ETH received
        if(msg.value > predictedAmounts[0]){
          TransferHelper.transferETH(_to, msg.value - predictedAmounts[0]);  
        }
    }

    /*
        Swap's the tokens in for exact ETH out. Fails if the required tokens in are greater
        than _amountInMax. It allows mutiple swaps in the order of the param _path.
    */
    function swapTokensInForExactETH(
        uint _amountOut,
        uint _amountInMax,
        address[] calldata _path,
        address _to,
        uint _deadline
    ) external isActive(_deadline) returns(uint[] memory amounts){
        require(_path[_path.length - 1] == WETH, "Invalid Path");

        // Get the predicted amounts and checks if it is lesser than _amountInMax.
        uint[] memory predictedAmounts = SwapHelper.getAmountsIn(factory ,_amountOut, _path);
        require(predictedAmounts[0] <= _amountInMax, "Exceeds the maximum input tokens");
        
        // Swap the tokens according to specified path and predicted amountIn
        amounts = _swap(predictedAmounts[0], _path, msg.sender, address(this));

        // Withdraw the WETH and transfer to the _to address
        IWrappedEther(WETH).withdraw(amounts[_path.length - 1]);
        TransferHelper.transferETH(_to, amounts[_path.length - 1]);
    }

    /*
        The function to add liquidity. It gets the token pair of the combination, if not available creates it.
        Then finds the optimal tokens that can be added and mints the liquidity shares.  
    */
    function addLiquidity(
        address _tokenA,
        address _tokenB,
        uint _amountADesired,
        uint _amountBDesired,
        uint _amountAMin,
        uint _amountBMin,
        address _to,
        uint _deadline
    ) external isActive(_deadline) returns(uint liquidity) {
        // Check the tokens and get the pair 
        address pair = _checkTokensAndGetPair(_tokenA, _tokenB, true);
        
        // Get the amount of optimal tokens to add 
        (uint amountA, uint amountB) = _getOptimalTokensToAdd(
            _tokenA,
            _tokenB,
            _amountADesired,
            _amountBDesired,
            _amountAMin,
            _amountBMin,
            pair
        );

        // Transfer the optimal tokens to the pair 
        TransferHelper.transferFrom(_tokenA, msg.sender, pair, amountA, "Failed to transfer token A");
        TransferHelper.transferFrom(_tokenB, msg.sender, pair, amountB, "Failed to transfer token B");

        // Mint shares
        (uint amount0, uint amount1) = (_tokenA < _tokenB) ? (amountA, amountB) : (amountB, amountA); 
        return IPair(pair).mint(_to, amount0, amount1);
    }

    /*
        The function to add liquidity. It gets the token pair of the combination, if not available creates it.
        Then finds the optimal tokens that can be added and mints the liquidity shares.  
    */
    function removeLiquidity(
        address _tokenA,
        address _tokenB,
        uint _liquidity,
        uint _amountAMin,
        uint _amountBMin,
        address _to,
        uint _deadline
    ) external isActive(_deadline) returns(uint amountA, uint amountB){
        // Check the tokens and get the pair 
        address pair = _checkTokensAndGetPair(_tokenA, _tokenB, false);

        // Transfer liquidity to pair
        TransferHelper.transferFrom(pair, msg.sender, pair, _liquidity, "Failed to transfer liquidity");

        // Burn the liquidity and get the token amounts 
        (uint amount0, uint amount1) = IPair(pair).burn(_to, _liquidity);
        
        // Revert if the tokens are less than the minimum expected 
        (amountA, amountB) = _tokenA < _tokenB ? (amount0, amount1) : (amount1, amount0);
        require(amountA > _amountAMin, "TokenA less the min");
        require(amountB > _amountBMin, "TokenB less the min");
    }

    // Returns the amounts of tokens out for the token amount in. 
    function getAmountsOut(uint _amountIn, address[] calldata _path) public view returns(uint[] memory) {
        return SwapHelper.getAmountsOut(factory, _amountIn, _path);
    }
    
    // Returns the amounts of tokens in for the token amount out. 
    function getAmountsIn(uint _amountOut, address[] calldata _path) public view returns(uint[] memory) {
        return SwapHelper.getAmountsIn(factory, _amountOut, _path);
    }

    /*
        Returns the optimal tokens to be add liquidity 
    */
    function _getOptimalTokensToAdd(
        address _tokenA,
        address _tokenB,
        uint _amountADesired,
        uint _amountBDesired,
        uint _amountAMin,
        uint _amountBMin,
        address pair
    ) private returns(uint amountA, uint amountB) {
        // Get reserves
        (uint reserveA, uint reserveB) = IPair(pair).getReserves(_tokenA, _tokenB);

        // Find the optimal amount of tokens
        if(reserveA == 0 && reserveB == 0){
            (amountA, amountB) = (_amountADesired, _amountBDesired);
        } else{
            uint amountBOptimal = _quote(_amountADesired, reserveA, reserveB);
            if(amountBOptimal <= _amountBDesired){
                require(amountBOptimal > _amountBMin, "Insufficient amount B");
                (amountA, amountB) = (_amountADesired, amountBOptimal);
            } else{
                uint amountAOptimal = _quote(_amountBDesired, reserveB, reserveA);
                require(amountAOptimal <= _amountBDesired);
                require(amountAOptimal >= _amountAMin, "Insufficient amount A");
                (amountA, amountB) = (amountAOptimal, _amountBDesired);
            }
        } 
    }

    /*
        Returns the no of Btoken's needed if no of AToken's and reserve's are provided.   
    */
    function _quote(uint amountA, uint reserveA, uint reserveB) private pure returns(uint amountB){
        // dx/dy = X/Y 
        amountB = (amountA * reserveB) / reserveA;
    }

    /*
        Validates the tokens and returns the pair. Create's a new pair if the pair doesn't exists
        and if _createNewPair param is true.
    */
    function _checkTokensAndGetPair(address _tokenA, address _tokenB, bool _createNewPair) private returns(address pair) {
        // Validate the tokens
        require(_tokenA != address(0) && _tokenB != address(0), "Tokens are not valid");
        require(_tokenA != _tokenB, "Both tokens are identical");

        // Get the pair
        pair = IFactory(factory).getPair(_tokenA, _tokenB);

        // Create a pair if needed
        if(pair == address(0) && _createNewPair){
            return IFactory(factory).createPair(_tokenA, _tokenB);
        }

        require(pair != address(0), "Pair doesn't exist");
    }

    /* 
        Swap the tokens according to the _amountIn and _path
    */
    function _swap(uint _amountIn, address[] calldata _path, address _from, address _to) private returns (uint[] memory amounts) {
        amounts = new uint[](_path.length);
        amounts[0] = _amountIn;

        // Get pair and transfer the amountIn tokens to pair
        address firstPair = SwapHelper.getPair(factory, _path[0], _path[1]);
        TransferHelper.transferFrom(_path[0], _from, firstPair, amounts[0], "Failed to transfer tokens");
        
        // Iterate through the path, get pair, to and execute swap
        for(uint i; i < (_path.length - 1); i++){
            address pair = SwapHelper.getPair(factory, _path[i], _path[i+1]);
            address to = (i < _path.length - 2) ? (SwapHelper.getPair(factory, _path[i+1], _path[i+2])) : _to;
            amounts[i+1] = IPair(pair).swap(_path[i], amounts[i], to);
        }
    }
}