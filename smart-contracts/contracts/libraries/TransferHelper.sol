// SPDX-License-Identifier: MIT
pragma solidity 0.8.17;

import "../interfaces/IERC20Token.sol";

library TransferHelper {

    function transferFrom(address _token, address _from, address _to, uint _amount, string memory errorMessage) internal{
        require(IERC20Token(_token).transferFrom(_from, _to, _amount), errorMessage);
    }

    function transfer(address _token, address _to, uint _amount, string memory errorMessage) internal{
        require(IERC20Token(_token).transfer(_to, _amount), errorMessage);
    }

    function transferETH(address _to, uint _amount) internal {
        (bool sent,) = payable(_to).call{value: _amount}("");
        require(sent, "Failed to send ether");
    }    

}