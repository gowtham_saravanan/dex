// SPDX-License-Identifier: MIT
pragma solidity 0.8.17;
import '../interfaces/IFactory.sol';
import '../interfaces/IPair.sol';
import '../Pair.sol';

library SwapHelper {

    /*
        Computes the address of the pair.
    */
    function getPair(address factory, address _tokenA, address _tokenB) internal view returns(address pair) {
        // sort the tokens
        (address token0, address token1) = (_tokenA < _tokenB) ? (_tokenA, _tokenB) : (_tokenB, _tokenA);

        // compute the address of the contract created using bytecode, salt and factory address 
        bytes memory bytecode = type(Pair).creationCode;
        bytes32 _salt = keccak256(abi.encodePacked(token0, token1));
        bytes32 hash = keccak256(
            abi.encodePacked(bytes1(0xff), factory, _salt, keccak256(bytecode))
        );

        pair = address(uint160(uint(hash)));
    }

    /*
        Returns the amount of tokens out for the specified tokens in with fee. 
    */
    function getAmountOut(uint _amountIn, uint _reserveIn, uint _reserveOut) internal pure returns(uint){
        // 0.3% fee, so balance 99.7% tokens are counted. 
        uint amountInWithFee = (_amountIn * 997) / 1000;
        
        // dy = Ydx/X+dx;
        return (_reserveOut * amountInWithFee)/(_reserveIn + amountInWithFee);
    }

    /*
        Returns the amount of tokens in needed with fee for the specified tokens out. 
    */
    function getAmountIn(uint _amountOut, uint _reserveIn, uint _reserveOut) internal pure returns(uint){    
        // dx = Xdy/Y-dy;
        uint amountOut = (_reserveIn * _amountOut)/(_reserveOut - _amountOut);

        // With 0.3% fees
        return ((amountOut * 1000) / 997) + 2;
    }

    /*
        Finds the amounts of tokensOut in each swap according to the _path and _amountIn given.  
    */
    function getAmountsOut(address factory, uint _amountIn, address[] calldata _path) internal view returns(uint[] memory amounts){
        // Require minimum of 2 paths
        require(_path.length >= 2, "Invalid no of paths");
    
        amounts = new uint[](_path.length);
        amounts[0] = _amountIn;

        // Loop through the path and find the expected amount out
        for(uint i; i < (_path.length - 1); i++){
            require(amounts[i] > 0, "Insufficient Amount In");

            // Get pair
            address pair = IFactory(factory).getPair(_path[i], _path[i+1]);
            require(pair != address(0), "Pair does not exist"); 

            // Get reserves
            (uint reserve0, uint reserve1) = IPair(pair).getReserves(_path[i], _path[i+1]);
            require(reserve0 > 0 && reserve1 > 0, "Insufficient Liquidity");

            // Get amount out
            amounts[i+1] = getAmountOut(amounts[i], reserve0, reserve1);
        }
    }

    /*
        Finds the amounts of tokensIn in each swap according to the _path and _amountOut given.  
    */
    function getAmountsIn(address factory, uint _amountOut, address[] calldata _path) internal view returns(uint[] memory amounts){
        // Require minimum of 2 paths
        require(_path.length >= 2, "Invalid no of paths");
        
        amounts = new uint[](_path.length);
        amounts[_path.length - 1] = _amountOut;
        
        // Loop through the path from reverse and find the expected amount in
        for(uint i = _path.length - 1; i > 0 ; i--){
            require(amounts[i] > 0, "Insufficient amount out");

            // Get pair
            address pair = IFactory(factory).getPair(_path[i], _path[i-1]);
            require(pair != address(0), "Pair does not exist"); 
            
            // Get reserves
            (uint reserve0, uint reserve1) = IPair(pair).getReserves(_path[i-1], _path[i]);
            require(reserve0 > 0 && reserve1 > 0, "Insufficient Liquidity");
        
            // Get amount in
            amounts[i-1] = getAmountIn(amounts[i], reserve0, reserve1);
        }
    }
    
    /*
        Compares the string by hashing it.
    */
    function compareStrings(string memory a, string memory b) internal pure returns (bool) {
       return (keccak256(abi.encodePacked((a))) == keccak256(abi.encodePacked((b))));
    }
}