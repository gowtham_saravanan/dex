const {expect} = require("chai");
const {ethers} = require("hardhat");
const { loadFixture } = require("@nomicfoundation/hardhat-network-helpers");

describe("Router", () => {
    
    let USDC = "0xa0b86991c6218b36c1d19d4a2e9eb0ce3606eb48";
    let USDT = "0xa0b86991c6218b36c1d19d4a2e9eb0ce3606eb40"; //Dummy Token
    let WETH = "0xC02aaA39b223FE8D0A0e5C4F27eAD9083C756Cc2";

    async function initializeContracts() {
        let account1, account2;
        let router, factory, uniswapV2Swap, weth, usdc;
        [account1, account2] = await ethers.getSigners();

        let Factory = await ethers.getContractFactory("Factory");
        factory = await Factory.deploy();

        let Router = await ethers.getContractFactory("Router");
        router = await Router.deploy(factory.address, WETH);

        let UniswapV2Swap = await ethers.getContractFactory("UniswapV2Swap");
        uniswapV2Swap = await UniswapV2Swap.deploy();

        weth = await ethers.getContractAt("IWrappedEther", WETH);
        usdc = await ethers.getContractAt("ERC20Token", USDC);

        return {router, factory, uniswapV2Swap, weth, usdc, account1, account2};
    }
    
    async function removePreviousTokenBalances(weth, usdc, account1, account2, factory) {
        await weth.transfer(account2.address, await weth.balanceOf(account1.address));
        await usdc.transfer(account2.address, await usdc.balanceOf(account1.address));

        let pairAddress = await factory.getPair(WETH, USDC);
        if(pairAddress != ethers.constants.AddressZero){
            let pair = await ethers.getContractAt("Pair", pairAddress);            
            await pair.transfer(account2.address, await pair.balanceOf(account1.address));
        }
    }

    async function depositWeth(weth, amount){
        await weth.deposit({value: amount});
    }

    async function swapTokensUsingUniswap(weth, uniswapV2Swap){
        await weth.approve(uniswapV2Swap.address, ethers.utils.parseEther('10'));
        await uniswapV2Swap.singleSwapExactAmountIn(ethers.utils.parseEther('10'), 1, {
            gasLimit: 1000000
        });
    }

    async function addLiquidity(router, factory, account1, token1, token2, amount1, amount2) {
        let block = await ethers.provider.getBlockNumber();
        let timestamp = (await ethers.provider.getBlock(block)).timestamp + 100;

        await token1.approve(router.address, amount1);
        await token2.approve(router.address, amount2);

        txn = await router.addLiquidity(
            token1.address,
            token2.address,
            amount1,
            amount2,
            1,
            1,
            account1.address,
            timestamp, {
                gasLimit: 30000000
        });

        let pairAddress = await factory.getPair(token1.address, token2.address);
        let pair = await ethers.getContractAt("Pair", pairAddress);
        let liquidity = await pair.balanceOf(account1.address);
        return {liquidity};
    }

    async function resetBalancesAndLiquidity(weth, usdc, account1, account2, uniswapV2Swap, factory, router){
        await removePreviousTokenBalances(weth, usdc, account1, account2, factory);
        await depositWeth(weth, ethers.utils.parseEther('20'));
        await swapTokensUsingUniswap(weth, uniswapV2Swap);
        return await addLiquidity(router, factory, account1, weth, usdc, ethers.utils.parseEther("1"), 1000*(10**6))
    }

    it("Deployed successfully", async () => {
        const {router, factory} = await loadFixture(initializeContracts)
        expect(await router.factory()).to.be.equal(factory.address);
    });

    it("Deposit WETH tokens works", async() => {
        const {weth, usdc, account1, account2, factory} = await loadFixture(initializeContracts)
        await removePreviousTokenBalances(weth, usdc, account1, account2, factory);

        expect(await weth.balanceOf(account1.address)).to.be.equal(0);
        await depositWeth(weth, ethers.utils.parseEther("20"));
        expect(await weth.balanceOf(account1.address)).to.be.equal(ethers.utils.parseEther('20'));
    });

    it("Swap WETH tokens for USDC using uniswap for inital liquidity works", async () => {
        const {weth, usdc, account1, uniswapV2Swap, account2, factory} = await loadFixture(initializeContracts)
        await removePreviousTokenBalances(weth, usdc, account1, account2, factory);
        await depositWeth(weth, ethers.utils.parseEther('10'));

        expect(await usdc.balanceOf(account1.address)).to.be.equal(0);
        await swapTokensUsingUniswap(weth, uniswapV2Swap);
        expect(await usdc.balanceOf(account1.address)).to.be.greaterThan(10);
    });

    it("Add Liquidity works", async () => {
        const {weth, usdc, account1, uniswapV2Swap, account2, factory, router} = await loadFixture(initializeContracts)
        
        await removePreviousTokenBalances(weth, usdc, account1, account2, factory);
        await depositWeth(weth, ethers.utils.parseEther('20'));
        await swapTokensUsingUniswap(weth, uniswapV2Swap);

        let wethBalance = await weth.balanceOf(account1.address);
        let usdcBalance = await usdc.balanceOf(account1.address);
        let wethAmount = ethers.utils.parseEther("1");
        let usdcAmount = 1000*(10**6);

        await expect(
            addLiquidity(router, factory, account1, weth, weth, 0, usdcAmount)
        ).to.be.revertedWith("Both tokens are identical");

        await expect(
            addLiquidity(router, factory, account1, weth, usdc, 0, usdcAmount)
        ).to.be.revertedWith("Not enough tokens provided")
        
        let {liquidity} = await addLiquidity(router, factory, account1, weth, usdc, wethAmount, usdcAmount);
        
        // (sqrt(amountA * amountB)) since totalSupply = 0
        let expectedLiquidity = Math.floor(Math.sqrt(wethAmount * usdcAmount));
        expect(liquidity.toNumber()).equals(expectedLiquidity);

        let pairAddress = await factory.getPair(WETH, USDC);
        let pair = await ethers.getContractAt("Pair", pairAddress);
        let reserves = await pair.getReserves(WETH, USDC);
        expect(wethBalance).greaterThan(await weth.balanceOf(account1.address));
        expect(usdcBalance).greaterThan(await usdc.balanceOf(account1.address));
        expect(reserves[0]).equals(wethAmount);
        expect(reserves[1]).equals(usdcAmount);
        
        wethBalance = await weth.balanceOf(account1.address);
        usdcBalance = await usdc.balanceOf(account1.address);
        wethAmount = ethers.utils.parseEther("0.1");
        usdcAmount = 100*(10**6);
        
        let data = await addLiquidity(router, factory, account1, usdc, weth, usdcAmount, wethAmount)
      
        // s = (aT/A), A = ethers.utils.parseEther('1'), T = liquidity (shares minted only once)
        expectedLiquidity += Math.floor((ethers.utils.parseEther('0.1') / ethers.utils.parseEther('1')) * liquidity) ;
        liquidity = data.liquidity;
        expect(liquidity).equals(expectedLiquidity);

        reserves = await pair.getReserves(WETH, USDC);
        expect(wethBalance).greaterThan(await weth.balanceOf(account1.address));
        expect(usdcBalance).greaterThan(await usdc.balanceOf(account1.address));
        
        expect(reserves[0]).equals(ethers.utils.parseEther("1.1"));
        expect(reserves[1]).equals(1100*(10**6));

    });

    it("Swap Exact Tokens In For Tokens Works", async () => {
        const {weth, usdc, account1, account2, uniswapV2Swap, factory, router} = await loadFixture(initializeContracts)
        
        let {liquidity} = await resetBalancesAndLiquidity(weth, usdc, account1, account2, uniswapV2Swap, factory, router);

        let pairAddress = await factory.getPair(WETH, USDC);
        let pair = await ethers.getContractAt("Pair", pairAddress);
        let reserves = await pair.getReserves(WETH, USDC);
        expect(reserves[0]).equals(ethers.utils.parseEther("1"));
        expect(reserves[1]).equals(1000*(10**6));
        
        let swapAmount = ethers.utils.parseEther('0.1');
        let block = await ethers.provider.getBlockNumber();
        let timestamp = (await ethers.provider.getBlock(block)).timestamp + 100;

        await weth.approve(router.address, ethers.utils.parseEther('2'));

        await expect(
            router.swapExactTokensInForTokens(swapAmount, 1, [WETH], account1.address, timestamp,
                { gasLimit: 30000000 })
        ).to.be.revertedWith("Invalid no of paths");
        
        await expect(
            router.swapExactTokensInForTokens(0, 1, [WETH, USDC], account1.address, timestamp,
                { gasLimit: 30000000 })
        ).to.be.revertedWith("Insufficient Amount In");

        await expect(
            router.swapExactTokensInForTokens(swapAmount, 1, [WETH, USDT], account1.address, timestamp,
                { gasLimit: 30000000 })
        ).to.be.revertedWith("Pair does not exist");
        
        await expect(
            router.swapExactTokensInForTokens(swapAmount, 200 * 10**6, [WETH, USDC], account1.address, 
                timestamp, { gasLimit: 30000000 })
        ).to.be.revertedWith("Insufficient amount out tokens");
        
        await router.swapExactTokensInForTokens(swapAmount, 1, [WETH, USDC], account1.address, timestamp,
            { gasLimit: 30000000 });

        reserves = await pair.getReserves(WETH, USDC);
        expect(reserves[0]).equals(ethers.utils.parseEther("1.1"));
        expect(reserves[1]).lessThan(1000*(10**6));
        
        await pair.approve(router.address, liquidity);
        txn = await router.removeLiquidity(USDC, WETH, liquidity, 1, 1, account1.address,
            timestamp, { gasLimit: 30000000});
        await txn.wait();
        
        await expect(
            router.swapExactTokensInForTokens(swapAmount, 1, [WETH, USDC], account1.address, timestamp,
                { gasLimit: 30000000 })
        ).to.be.revertedWith("Insufficient Liquidity");
    });

    it("Swap Exact ETH In for Tokens Works", async () => {
        const {weth, usdc, account1, uniswapV2Swap, account2, factory, router} = await loadFixture(initializeContracts)
        
        let {liquidity} = await resetBalancesAndLiquidity(weth, usdc, account1, account2, uniswapV2Swap, factory, router);

        let pairAddress = await factory.getPair(WETH, USDC);
        let pair = await ethers.getContractAt("Pair", pairAddress);
        let reserves = await pair.getReserves(WETH, USDC);
        expect(reserves[0]).equals(ethers.utils.parseEther("1"));
        expect(reserves[1]).equals(1000*(10**6));
        
        let swapAmount = ethers.utils.parseEther('0.1');
        let block = await ethers.provider.getBlockNumber();
        let timestamp = (await ethers.provider.getBlock(block)).timestamp + 100;
        
        await expect(
            router.swapExactETHInForTokens(1, [WETH], account1.address, timestamp,
                {value: swapAmount, gasLimit: 30000000 })
        ).to.be.revertedWith("Invalid no of paths");
        
        await expect(
            router.swapExactETHInForTokens(1, [USDC, WETH], account1.address, timestamp,
                {value: swapAmount, gasLimit: 30000000 })
        ).to.be.revertedWith("Invalid Path");

        await expect(
            router.swapExactETHInForTokens(1, [WETH, USDC], account1.address, timestamp,
                {value: 0, gasLimit: 30000000 })
        ).to.be.revertedWith("Insufficient Amount In");

        await expect(
            router.swapExactETHInForTokens(1, [WETH, USDT], account1.address, timestamp,
                {value: swapAmount, gasLimit: 30000000 })
        ).to.be.revertedWith("Pair does not exist");

        await expect(
            router.swapExactETHInForTokens(200 * 10**6, [WETH, USDC], account1.address, timestamp,
                {value: swapAmount, gasLimit: 30000000 })
        ).to.be.revertedWith("Insufficient amount out tokens");

        await router.swapExactETHInForTokens(1, [WETH, USDC], account1.address, timestamp,
            {value: swapAmount, gasLimit: 30000000 });
        
        reserves = await pair.getReserves(WETH, USDC);
        expect(reserves[0]).equals(ethers.utils.parseEther("1.1"));
        expect(reserves[1]).lessThan(1000*(10**6));
        
        await pair.approve(router.address, liquidity);
        await router.removeLiquidity(USDC, WETH, liquidity, 1, 1, account1.address,
            timestamp, { gasLimit: 30000000});
        await expect(
            router.swapExactETHInForTokens(1, [WETH, USDC], account1.address, timestamp,
                {value: swapAmount, gasLimit: 30000000 })
        ).to.be.revertedWith("Insufficient Liquidity");
    });

    it("Swap Exact Token In For ETH Works", async () => {
        const {weth, usdc, account1, uniswapV2Swap, account2, factory, router} = await loadFixture(initializeContracts)
        
        let {liquidity} = await resetBalancesAndLiquidity(weth, usdc, account1, account2, uniswapV2Swap, factory, router);

        let pairAddress = await factory.getPair(WETH, USDC);
        let pair = await ethers.getContractAt("Pair", pairAddress);
        let reserves = await pair.getReserves(WETH, USDC);
        expect(reserves[0]).equals(ethers.utils.parseEther("1"));
        expect(reserves[1]).equals(1000*(10**6));
        
        let swapAmount = 100 * (10**6);
        let block = await ethers.provider.getBlockNumber();
        let timestamp = (await ethers.provider.getBlock(block)).timestamp + 100;
        
        let ethbalance = await account1.getBalance(); 
        await usdc.approve(router.address, swapAmount);

        await expect(
            router.swapExactTokensInForETH(swapAmount, 1, [WETH], account1.address, timestamp,
                { gasLimit: 30000000 })
        ).to.be.revertedWith("Invalid no of paths");

        await expect(
            router.swapExactTokensInForETH(swapAmount, 1, [WETH, USDC], account1.address, timestamp,
                { gasLimit: 30000000 })
        ).to.be.revertedWith("Invalid Path");

        await expect(
            router.swapExactTokensInForETH(0, 1, [USDC, WETH], account1.address, timestamp,
                { gasLimit: 30000000 })
        ).to.be.revertedWith("Insufficient Amount In");

        await expect(
            router.swapExactTokensInForETH(swapAmount, 1, [USDT, WETH], account1.address, timestamp,
                { gasLimit: 30000000 })
        ).to.be.revertedWith("Pair does not exist");

        await expect(
            router.swapExactTokensInForETH(swapAmount, ethers.utils.parseEther("0.5"), [USDC, WETH], account1.address, 
                timestamp, { gasLimit: 30000000 })
        ).to.be.revertedWith("Insufficient amount out tokens");

        let txn = await router.swapExactTokensInForETH(swapAmount, 1, [USDC, WETH], account1.address,
            timestamp, { gasLimit: 30000000 }
        );
        await txn.wait();
        
        expect(await account1.getBalance()).greaterThan(ethbalance);
        reserves = await pair.getReserves(WETH, USDC);
        expect(reserves[0]).lessThan(ethers.utils.parseEther("1"));
        expect(reserves[1]).equals(1100*(10**6));

        await pair.approve(router.address, liquidity);
        await router.removeLiquidity(USDC, WETH, liquidity, 1, 1, account1.address,
            timestamp, { gasLimit: 30000000}
        );        
        await expect(
            router.swapExactTokensInForTokens(swapAmount, 1, [WETH, USDC], account1.address, timestamp,
                { gasLimit: 30000000 })
        ).to.be.revertedWith("Insufficient Liquidity");
    });

    it("Swap Tokens In For Exact Tokens Out Works", async () => {
        const {weth, usdc, account1, uniswapV2Swap, account2, factory, router} = await loadFixture(initializeContracts)
        
        let {liquidity} = await resetBalancesAndLiquidity(weth, usdc, account1, account2, uniswapV2Swap, factory, router);

        let pairAddress = await factory.getPair(WETH, USDC);
        let pair = await ethers.getContractAt("Pair", pairAddress);
        let reserves = await pair.getReserves(WETH, USDC);
        let previousUSDCBalance = await usdc.balanceOf(account1.address);

        let expectedUSDCAmount = ethers.BigNumber.from(100 * (10**6));
        let block = await ethers.provider.getBlockNumber();
        let timestamp = (await ethers.provider.getBlock(block)).timestamp + 100;
        let maxETHIn = ethers.utils.parseEther("0.2");
        await weth.approve(router.address, maxETHIn);
        
        await expect(
            router.swapTokensInForExactTokens(expectedUSDCAmount, maxETHIn, [WETH],
                account1.address, timestamp, { gasLimit: 30000000 })
        ).to.be.revertedWith("Invalid no of paths");

        await expect(
            router.swapTokensInForExactTokens(0, maxETHIn, [WETH, USDC],
                account1.address, timestamp, { gasLimit: 30000000 })
        ).to.be.revertedWith("Insufficient amount out");

        await expect(
            router.swapTokensInForExactTokens(expectedUSDCAmount, maxETHIn, [WETH, USDT],
                account1.address, timestamp, { gasLimit: 30000000 })
        ).to.be.revertedWith("Pair does not exist");

        await expect(
            router.swapTokensInForExactTokens(expectedUSDCAmount, ethers.utils.parseEther("0.001"),
             [WETH, USDC], account1.address, timestamp, { gasLimit: 30000000 })
        ).to.be.revertedWith("Exceeds the maximum input tokens");

        await router.swapTokensInForExactTokens(expectedUSDCAmount, maxETHIn, [WETH, USDC],
            account1.address, timestamp, { gasLimit: 30000000 });
        
        expect(await usdc.balanceOf(account1.address)).to.be.equal(previousUSDCBalance.add(expectedUSDCAmount));

        reserves = await pair.getReserves(WETH, USDC);
        expect(reserves[0]).greaterThan(ethers.utils.parseEther("1"));
        expect(reserves[1]).equals(900*(10**6));

        await pair.approve(router.address, liquidity);
        await router.removeLiquidity(USDC, WETH, liquidity, 1, 1, account1.address,
            timestamp, { gasLimit: 30000000}
        );
    
        await expect(
            router.swapTokensInForExactTokens(expectedUSDCAmount, ethers.utils.parseEther("0.001"),
             [WETH, USDC], account1.address, timestamp, { gasLimit: 30000000 })
        ).to.be.revertedWith("Insufficient Liquidity");
    });

    it("Swap ETH In For Exact Tokens Out Works", async () => {
        const {weth, usdc, account1, uniswapV2Swap, account2, factory, router} = await loadFixture(initializeContracts)
                  
        let {liquidity} = await resetBalancesAndLiquidity(weth, usdc, account1, account2, uniswapV2Swap, factory, router);

        let pairAddress = await factory.getPair(WETH, USDC);
        let pair = await ethers.getContractAt("Pair", pairAddress);
        let reserves = await pair.getReserves(WETH, USDC);
        
        let previousUSDCBalance = await usdc.balanceOf(account1.address);
        let expectedUSDCAmount = ethers.BigNumber.from(100 * (10**6));
        let maxETHIn = ethers.utils.parseEther('0.2');
        let accountBalance = await account1.getBalance();
        
        let block = await ethers.provider.getBlockNumber();
        let timestamp = (await ethers.provider.getBlock(block)).timestamp + 100;
        
        await expect(
            router.swapETHInForExactTokens( expectedUSDCAmount, [WETH], account1.address,
                timestamp, { value : maxETHIn, gasLimit: 30000000 })
        ).to.be.revertedWith("Invalid no of paths");      

        await expect(
            router.swapETHInForExactTokens( expectedUSDCAmount, [USDC, WETH], account1.address,
                timestamp, { value : maxETHIn, gasLimit: 30000000 })
        ).to.be.revertedWith("Invalid Path");

        await expect(
            router.swapETHInForExactTokens(0, [WETH, USDC], account1.address,
                timestamp, { value : maxETHIn, gasLimit: 30000000 })
        ).to.be.revertedWith("Insufficient amount out");

        await expect(
            router.swapETHInForExactTokens(expectedUSDCAmount, [WETH, USDT], account1.address,
                timestamp, { value : maxETHIn, gasLimit: 30000000 })
        ).to.be.revertedWith("Pair does not exist");

        await expect(
            router.swapETHInForExactTokens( expectedUSDCAmount, [WETH, USDC], account1.address,
                timestamp, { value : ethers.utils.parseEther("0.01"), gasLimit: 30000000 })
        ).to.be.revertedWith("Exceeds the maximum input tokens");

        await router.swapETHInForExactTokens( expectedUSDCAmount, [WETH, USDC], account1.address,
            timestamp, { value : maxETHIn, gasLimit: 30000000 }
        );
        expect(await usdc.balanceOf(account1.address)).to.be.equal(previousUSDCBalance.add(expectedUSDCAmount));
        
        reserves = await pair.getReserves(WETH, USDC);
        expect(accountBalance).greaterThan(await account1.getBalance());
        expect(reserves[0]).greaterThan(ethers.utils.parseEther("1"));
        expect(reserves[1]).equals(900*(10**6));

        await pair.approve(router.address, liquidity);
        await router.removeLiquidity(USDC, WETH, liquidity, 1, 1, account1.address,
            timestamp, { gasLimit: 30000000}
        );
        await expect(
            router.swapETHInForExactTokens(expectedUSDCAmount, [WETH, USDC], account1.address,
                timestamp, { value : maxETHIn, gasLimit: 30000000 })
        ).to.be.revertedWith("Insufficient Liquidity");
    });

    it("Swap Tokens In For Exact ETH Out Works", async () => {
        const {weth, usdc, account1, uniswapV2Swap, account2, factory, router} = await loadFixture(initializeContracts)
        
        let {liquidity} = await resetBalancesAndLiquidity(weth, usdc, account1, account2, uniswapV2Swap, factory, router);

        let pairAddress = await factory.getPair(WETH, USDC);
        let pair = await ethers.getContractAt("Pair", pairAddress);
        let reserves = await pair.getReserves(WETH, USDC);

        let accountBalance = await account1.getBalance();
        let expectedETHAmount = ethers.utils.parseEther("0.1");
        let maxUSDCAmount = 200 * (10**6)

        let block = await ethers.provider.getBlockNumber();
        let timestamp = (await ethers.provider.getBlock(block)).timestamp + 100;

        await usdc.approve(router.address, maxUSDCAmount);
        
        await expect(
            router.swapTokensInForExactETH(expectedETHAmount, maxUSDCAmount, [WETH], account1.address,
                timestamp, { gasLimit: 30000000 })
        ).to.be.revertedWith("Invalid no of paths");      

        await expect(
            router.swapTokensInForExactETH(expectedETHAmount, maxUSDCAmount, [WETH, USDC], account1.address,
                timestamp, { gasLimit: 30000000 })
        ).to.be.revertedWith("Invalid Path");

        await expect(
            router.swapTokensInForExactETH(0, maxUSDCAmount, [USDC, WETH], account1.address,
                timestamp, { gasLimit: 30000000 })
        ).to.be.revertedWith("Insufficient amount out");

        await expect(
            router.swapTokensInForExactETH(expectedETHAmount, maxUSDCAmount, [USDT, WETH], account1.address,
                timestamp, { gasLimit: 30000000 })
        ).to.be.revertedWith("Pair does not exist");

        await expect(
            router.swapTokensInForExactETH(expectedETHAmount, 10 * (10**6), [USDC, WETH], account1.address,
                timestamp, { gasLimit: 30000000 })
        ).to.be.revertedWith("Exceeds the maximum input tokens");

        await router.swapTokensInForExactETH(expectedETHAmount, maxUSDCAmount, [USDC, WETH],
            account1.address, timestamp, { gasLimit: 30000000 }
        );

        expect(accountBalance).lessThan(await account1.getBalance());

        reserves = await pair.getReserves(WETH, USDC);
        expect(reserves[0]).lessThan(ethers.utils.parseEther("1"));
        expect(reserves[1]).greaterThan(1000*(10**6));

        await pair.approve(router.address, liquidity);
        await router.removeLiquidity(USDC, WETH, liquidity, 1, 1, account1.address,
            timestamp, { gasLimit: 30000000}
        );
        await expect(
            router.swapTokensInForExactETH(expectedETHAmount, maxUSDCAmount, [USDC, WETH], account1.address,
                timestamp, { gasLimit: 30000000 })
        ).to.be.revertedWith("Insufficient Liquidity");
    });

    it("Remove liquidity works", async () => {
        const {weth, usdc, account1, uniswapV2Swap, account2, factory, router} = await loadFixture(initializeContracts)    
        let {liquidity} = await resetBalancesAndLiquidity(weth, usdc, account1, account2, uniswapV2Swap, factory, router);

        let pairAddress = await factory.getPair(WETH, USDC);
        let pair = await ethers.getContractAt("Pair", pairAddress);
        let reserves = await pair.getReserves(WETH, USDC);
        expect(reserves[0]).equals(ethers.utils.parseEther("1"));
        expect(reserves[1]).equals(1000*(10**6));
        let expectedLiquidity = Math.floor(Math.sqrt(ethers.utils.parseEther('1') *  1000*(10**6)));
        expect(liquidity).equals(expectedLiquidity);

        let wethBalance = await weth.balanceOf(account1.address);
        let usdcBalance = await usdc.balanceOf(account1.address);
        
        let block = await ethers.provider.getBlockNumber();
        let timestamp = (await ethers.provider.getBlock(block)).timestamp + 100;
        await pair.approve(router.address, liquidity);

        await expect(
            router.removeLiquidity(WETH, WETH, liquidity, 1, 1, account1.address,
                timestamp, { gasLimit: 30000000 })
        ).to.be.revertedWith("Both tokens are identical");

        await expect(
            router.removeLiquidity(WETH, WETH, liquidity - 100, 1, 1, account1.address,
                timestamp, { gasLimit: 30000000 })
        ).to.be.reverted;

        txn = await router.removeLiquidity(USDC, WETH, liquidity, 1, 1, account1.address,
                timestamp, { gasLimit: 30000000 });

        await txn.wait();
        
        liquidity = await pair.balanceOf(account1.address);
        expect(liquidity).equals(0);
        reserves = await pair.getReserves(WETH, USDC);
        expect(reserves[0]).equals(0);
        expect(reserves[1]).equals(0);

        expect(wethBalance).lessThan(await weth.balanceOf(account1.address));
        expect(usdcBalance).lessThan(await usdc.balanceOf(account1.address));
    });

    it("Get amount out works", async() => {
        const {weth, usdc, account1, uniswapV2Swap, account2, factory, router} = await loadFixture(initializeContracts)    
        let {liquidity} = await resetBalancesAndLiquidity(weth, usdc, account1, account2, uniswapV2Swap, factory, router);

        let pairAddress = await factory.getPair(WETH, USDC);
        let pair = await ethers.getContractAt("Pair", pairAddress);
        
        let amountIn = ethers.utils.parseEther("0.1");
        
        await expect(router.getAmountsOut(amountIn, [WETH])).to.be.revertedWith("Invalid no of paths");
        await expect(router.getAmountsOut(0, [WETH, USDC])).to.be.revertedWith("Insufficient Amount In");
        await expect(router.getAmountsOut(amountIn, [WETH, USDT])).to.be.revertedWith("Pair does not exist");

        let amounts = await router.getAmountsOut(amountIn, [WETH, USDC]); 
        expect(amounts[0]).to.be.equal(amountIn);
        expect(amounts[1]).to.be.lessThan(100 * 10**6);
        expect(amounts[1]).to.be.greaterThan(10 * 10**6);

        let block = await ethers.provider.getBlockNumber();
        let timestamp = (await ethers.provider.getBlock(block)).timestamp + 100;
        await pair.approve(router.address, liquidity);
        await router.removeLiquidity(USDC, WETH, liquidity, 1, 1, account1.address,
            timestamp, { gasLimit: 30000000}
        );
        await expect(router.getAmountsOut(amountIn, [WETH, USDC])).to.be.revertedWith("Insufficient Liquidity");
    });
 
    it("Get amount in works", async() => {
        const {weth, usdc, account1, uniswapV2Swap, account2, factory, router} = await loadFixture(initializeContracts)    
        let {liquidity} = await resetBalancesAndLiquidity(weth, usdc, account1, account2, uniswapV2Swap, factory, router);

        let pairAddress = await factory.getPair(WETH, USDC);
        let pair = await ethers.getContractAt("Pair", pairAddress);
        
        let amountOut = 100 * 10**6;
        
        await expect(router.getAmountsIn(amountOut, [WETH])).to.be.revertedWith("Invalid no of paths");
        await expect(router.getAmountsIn(0, [WETH, USDC])).to.be.revertedWith("Insufficient amount out");
        await expect(router.getAmountsIn(amountOut, [WETH, USDT])).to.be.revertedWith("Pair does not exist");

        let amounts = await router.getAmountsIn(amountOut, [WETH, USDC]); 
        expect(amounts[1]).to.be.equal(amountOut);
        expect(amounts[0]).to.be.lessThan(ethers.utils.parseEther("1"));
        expect(amounts[0]).to.be.greaterThan(ethers.utils.parseEther("0"));

        let block = await ethers.provider.getBlockNumber();
        let timestamp = (await ethers.provider.getBlock(block)).timestamp + 100;
        await pair.approve(router.address, liquidity);
        await router.removeLiquidity(USDC, WETH, liquidity, 1, 1, account1.address,
            timestamp, { gasLimit: 30000000}
        );
        await expect(router.getAmountsIn(amountOut, [WETH, USDC])).to.be.revertedWith("Insufficient Liquidity");
    });

});